" Spenser Pothier vimrc
set nocompatible

" Colors {{{
set t_Co=256
colorscheme badwolf
"}}}

" UI Layout {{{
set number
set nobackup
set nowritebackup
set noswapfile
set shortmess+=I
set wildmenu
set showmatch
set cursorline
set incsearch
syntax enable

" Set spellcheck for markdown and gitcommit files
autocmd BufRead,BufNewFile *.md set filetype=markdown
autocmd FileType gitcommit setlocal spell

" Set json for .template files as I'm usually doing CFN templates which are
" json
autocmd BufRead,BufNewFile *.template set filetype=json

" Set vertical rulers at column 80 and 100
set colorcolumn=80,100
highlight ColorColumn ctermbg=DarkGray

" Font stuff for vim-airline
set guifont=Liberation\ Mono\ for\ Powerline\ 10
let g:airline_powerline_fonts = 1

" Set different explore mode style
let g:netrw_liststyle=3
let g:netrw_winsize=-28
let g:netrw_banner=0
let g:netrw_browse_split=4

set mouse=a
"}}}

" Searching {{{
set hlsearch
"}}}

" Spaces and Tabs {{{
" Spaces instead of tabs
set expandtab
set smarttab
" tab width to 2
set shiftwidth=2
set tabstop=2

" Auto indent, smart indent
set ai
set si
syntax on
filetype on
filetype indent on
filetype plugin on
filetype plugin indent on
setlocal omnifunc=syntaxcomplete#Complete

"}}}

" Folding {{{
set foldenable
" }}}

" Key Bindings {{{

" Remap leader to ","
let mapleader=","

" Toggle folding with space in normal mode
nnoremap <space> za

" Force use of hjkl for navigation in normal mode
noremap <Up> :echoerr "Use k dummy"<CR>
noremap <Down> :echoerr "Use j dummy"<CR>
noremap <Left> :echoerr "Use h dummy"<CR>
noremap <Right> :echoerr "Use l dummy"<CR>

"Clear search highlighting with <Leader> and return
nnoremap <Leader><CR> :nohlsearch<CR>

" Shortcuts to edit and source vimrc file
nnoremap <Leader>sv :source $MYVIMRC<CR>
nnoremap <Leader>ev :e $MYVIMRC<CR>

nnoremap ; :

" Convert document to pdf (using pandoc)
nnoremap <Leader>md :! pandoc %:t -o %:t:r.pdf

" Explore mode
map <Leader>e :E<cr>

" Tab switching
map <Leader>[ :tabp<cr>
map <Leader>] :tabn<cr>

" Use ctags + ctrlP = magic
nnoremap <leader>. :CtrlPTag<cr>

" Use jk in insertmode to escape
inoremap jk <Esc>

" Fix some weird backspace issues in OSX, shouldn't have any adverse affects
" in linux
set backspace=indent,eol,start

" Set CTRL+[hjkl] to navigate windows
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" }}}

" Plugins {{{
" Load pathogen
execute pathogen#infect()

source ~/.vim/bundle/supertab/plugin/supertab.vim

set laststatus=2

let g:gitgutter_avoid_cmd_prompt_on_windows = 0

"let g:ackprg = 'ag'

"}}}

" Functions {{{

" Pretty Print JSON
com! PPJson %!jq '.'

" Validate CFN template
com! Validatecfn !aws cloudformation validate-template --template-body file:////%:p

"}}}

" Vimrc Folding Stuff {{{
set modeline
set modelines=1

" }}}


" Golang stuff {{{

let g:neocomplete#enable_at_startup = 1
let g:go_disable_autoinstall = 0

" Highlight
let g:go_highlight_functions = 1  
let g:go_highlight_methods = 1  
let g:go_highlight_structs = 1  
let g:go_highlight_operators = 1  
let g:go_highlight_build_constraints = 1  

let g:tagbar_type_go = {  
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
\ }

nmap <F8> :TagbarToggle<CR>
let g:SuperTabClosePreviewOnPopupClose = 1


set completeopt-=preview
let g:go_fmt_command = "goimports"
" }}}

" vim:foldmethod=marker:foldlevel=0
