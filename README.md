# Description

The dotfiles repository of Spenser Pothier

## Configs being managed

* Vim
* Zsh
* Git

## Plugins included:

### Vim

* vim-airline
* vim-gitgutter
* ctrlp
* supertab
* vim-ags
* vim-fugitive
* vim-ruby
* vim-rubocop

## Todo:

- [ ] Write this
- [ ] Write shell script to auto-symlink
